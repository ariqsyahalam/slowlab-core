from django.db import models

# Create your models here.
class DataCovid(models.Model):
    name = models.CharField(max_length=40)
    total_kasus = models.CharField(max_length=10)
    meninggal = models.CharField(max_length=10)
    sembuh = models.CharField(max_length=10)
    kasus_aktif = models.CharField(max_length=10)
    last_date = models.CharField(max_length=15)
    penambahan_tk = models.CharField(max_length=10)
    penambahan_m = models.CharField(max_length=10)
    penambahan_s = models.CharField(max_length=10)
    penambahan_ka = models.CharField(max_length=10)

    def __str__(self):
        return self.name
