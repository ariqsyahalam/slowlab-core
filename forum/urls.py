from django.urls import path
from .views import index, getForum
urlpatterns = [
    path('', index, name='index'),
    path('getForum/', getForum, name='getForum')
]