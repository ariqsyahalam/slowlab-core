from django.shortcuts import render, get_object_or_404
from .models import Product
from django.http import JsonResponse
from django.http.response import HttpResponse
from django.core import serializers


# Create your views here.
def main(request):
    products = Product.objects.all
    response = {'products': products}
    return render(request, 'main copy.html', response)

def product_detail(request, pk):
    obj = get_object_or_404(Product, pk=pk)
    return render(request, 'detail_produk copy.html', {'obj': obj})

def json(request):
    products = Product.objects.all
    data = serializers.serialize('json', Product.objects.all())
    return HttpResponse(data, content_type="application/json")

def search_results(request):
    if request.is_ajax():
        res = None
        produk = request.POST.get('produk')
        qs = Product.objects.filter(name__icontains=produk)
        if len(qs) > 0 and len(produk) > 0:
            data = []
            for pos in qs:
                item = {
                    'pk': pos.pk,
                    'name': pos.name,
                    'description': pos.description,
                    'price': pos.price
                }
                data.append(item)
            res = data
        else:
            res = 'Kami tidak dapat menemukan apa yang kamu cari'

        return JsonResponse({'data': res})
    return JsonResponse({})

def search_results_detail(request, pk):
    if request.is_ajax():
        res = None
        produk = request.POST.get('produk')
        qs = Product.objects.filter(name__icontains=produk)
        if len(qs) > 0 and len(produk) > 0:
            data = []
            for pos in qs:
                item = {
                    'pk': pos.pk,
                    'name': pos.name,
                    'description': pos.description,
                    'price': pos.price
                }
                data.append(item)
            res = data
        else:
            res = 'Kami tidak dapat menemukan apa yang kamu cari'

        return JsonResponse({'data': res})
    return JsonResponse({})