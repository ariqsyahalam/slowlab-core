from django.db import models

# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(null = True, max_length = 200)
    price = models.PositiveIntegerField()

    def __str__(self):
        return str(self.name)
    