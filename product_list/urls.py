from django.urls import path
from .views import main, product_detail, search_results, search_results_detail, json

urlpatterns = [
    path('', main, name='main'),
    path('search/', search_results, name='search'),
    path('<pk>/search/', search_results_detail, name='search_detail'),
    path('<pk>/', product_detail, name='detail'),
    path('json',json)
]