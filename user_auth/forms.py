from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms import fields
from django.forms.models import ModelForm

from user_auth.models import user_profile

class profile_form(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["province"].widget.attrs.update({
            'type':'province',
            'name':'province',
            'class':'form-control',
            'style':'border-radius: 15px;',
            'id':'province',
            'placeholder':'province'
        })

    class Meta:
        model = user_profile
        fields = '__all__'
        exclude = ['user']


class user_regist_form(UserCreationForm):
    email = forms.EmailField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["password1"].widget.attrs.update({
            'type':'password1',
            'name':'password1',
            'class':'form-control',
            'style':'border-radius: 15px;',
            'id':'password1',
            'placeholder':'Password'
        })
        self.fields["password2"].widget.attrs.update({
            'type':'password2',
            'name':'password2',
            'class':'form-control',
            'style':'border-radius: 15px;',
            'id':'password2',
            'placeholder':'Confirm Password'
        })
    
    class Meta:
        model = User
        fields = [
            "username",
            "email",
            "password1",
            "password2",
        ]
