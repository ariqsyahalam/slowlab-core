from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import SET_NULL

# Create your models here.
class Province(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class user_profile(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    full_name = models.CharField(max_length= 200, null= True)
    BOD = models.DateField(null = True)
    phone = models.CharField(max_length=20, null= True)
    province = models.ForeignKey(Province, on_delete=SET_NULL, blank= True, null= True)
    address = models.CharField(max_length= 500, null= True)
    
    def __str__(self) -> str:
        return self.user.username