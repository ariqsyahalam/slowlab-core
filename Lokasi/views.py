from django.db.models.base import Model
from django.forms import models
from django.http import response
from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse, JsonResponse
from django.core import serializers
import json
from .models import Lokasi
from .forms import FormLokasi

from django.views.decorators.csrf import csrf_exempt
import time

# Create your views here.
def index(request):
    lokasi = Lokasi.objects.all()
    response = {'lokasi':lokasi}
    return render(request, 'indexLokasi.html', response)

def add_lokasi(request):
    form = FormLokasi(request.POST or None)
    response = {'form':form}
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lokasi')
    return render(request, "add.html", response)

def get_lokasi(request):
    data = serializers.serialize('json', Lokasi.objects.all())
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def add_data(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        nama = data['nama']
        provinsi = data['provinsi']
        alamat = data['alamat']
        jam_buka = data['jam_buka']
        jam_tutup = data['jam_tutup']
        nomor = data['nomor']
        lokasis = Lokasi.objects.all()
        for lokasi in lokasis:
            if(lokasi.nama == nama or lokasi.alamat == alamat):
                return JsonResponse({"hasil": "lokasi gagal"}, status=400)
        lokasi_baru = Lokasi.objects.create()
        lokasi_baru.nama = nama
        lokasi_baru.provinsi = provinsi
        lokasi_baru.alamat = alamat
        lokasi_baru.jam_buka = jam_buka
        lokasi_baru.jam_tutup = jam_tutup
        lokasi_baru.nomor = nomor
        lokasi_baru.save()
        return JsonResponse({"hasil": "lokasi Dibuat"}, status=200)




