from django.db import models
# Create your models here.
class Lokasi(models.Model):
    nama = models.CharField(max_length=30)
    provinsi = models.CharField(max_length=30)
    alamat = models.CharField(max_length=200)
    jam_buka = models.CharField(max_length=5)
    jam_tutup = models.CharField(max_length=5)
    nomor = models.CharField(max_length=15)
