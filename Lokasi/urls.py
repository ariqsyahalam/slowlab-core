from django.urls import path
from .views import add_data, index,add_lokasi,get_lokasi,add_data
from django.conf.urls import url

urlpatterns = [
    path('', index, name='index'),
    path('add-lokasi/', add_lokasi, name='add_lokasi'),
    path('get-lokasi/', get_lokasi, name='get-lokasi'),
    path('add_data/', add_data, name = 'data_lokasi'),
]