from django.db import models

# Create your models here.
class Result(models.Model):
    cek_no_test = models.IntegerField(default=0)
    status = models.CharField(max_length=30)
    nama = models.CharField(max_length=30)
    no_ktp = models.IntegerField()
    tanggal_tes = models.DateField()

class Test(models.Model):
    no_test = models.IntegerField()
