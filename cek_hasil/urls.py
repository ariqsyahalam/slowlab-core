from django.urls import path, include

import cek_hasil
from .views import allData, index
from cek_hasil.viewset_api import *
from rest_framework import routers

router = routers.DefaultRouter()
router.register('result', ResultViewset)

urlpatterns = [
    path('', index, name='index'),
    path('all-data/', allData, name="all-data"),
    path('api/', include(router.urls)),
]