from django.apps import AppConfig


class CekHasilConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cek_hasil'
